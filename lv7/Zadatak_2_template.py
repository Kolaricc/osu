import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

# ucitaj sliku
img = Image.imread("imgs\\test_1.jpg")

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()

print(len(np.unique(img_array.reshape(-1, img.shape[2]), axis=0)))

km = KMeans(n_clusters=5, init='random', n_init=10, random_state=0)

km.fit(img_array)

labels = km.predict(img_array)

for index in range(len(img_array_aprox)):
    img_array_aprox[index] = km.cluster_centers_[labels[index]]

img_array_aprox = img_array_aprox.reshape(w,h,3)

plt.figure()
plt.title("Izmenjena slika")
plt.imshow(img_array_aprox)
plt.tight_layout()
plt.show()