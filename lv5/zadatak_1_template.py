import numpy as np
import matplotlib.pyplot as plt

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay, classification_report

X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

#a)
plt.scatter(X_train[:,0],X_train[:,1], c=y_train, cmap="RdBu")
plt.scatter(X_test[:,0],X_test[:,1], c=y_test, cmap="RdBu", marker='x')
plt.show()

#b)

LogRegression_model = LogisticRegression()
LogRegression_model.fit(X_train, y_train)

#c)
t0=LogRegression_model.intercept_
t1=LogRegression_model.coef_[0,0]
t2=LogRegression_model.coef_[0,1]

x=np.linspace(np.min(X_train[:,0]),np.max(X_train[:,0]))
y=-(t1/t2)*x -(t0/t2)
plt.scatter(X_train[:,0],X_train[:,1])
plt.plot(x,y,c="Green")
plt.show()

#d)
y_test_p= LogRegression_model.predict(X_test)
disp=ConfusionMatrixDisplay(confusion_matrix(y_test, y_test_p))
disp.plot()
plt.show()
print(classification_report(y_test, y_test_p))

#e)
pltColor=np.where(y_test-y_test_p==0,"Green", "Black")
plt.scatter(X_test[:,0],X_test[:,1],c=pltColor)
plt.show()