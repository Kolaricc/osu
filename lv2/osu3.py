import numpy as np
import matplotlib.pyplot as plt

img = plt.imread("lv2/road.jpg")
img = img[:,:,0].copy()
plt.figure()
plt.imshow(img, cmap="gray")
plt.show()
imgBright = img.copy()
lim = 255 - 50
imgBright[imgBright > lim] = 255
imgBright[imgBright <= lim] += 50
plt.imshow(imgBright, cmap="gray")
plt.show()

imgSlice=img[:,160:320].copy()
plt.imshow(imgSlice, cmap="gray")
plt.show()

imgRotate=img.copy()
imgRotate=imgRotate.transpose()
imgRotate=np.flip(imgRotate)
plt.imshow(imgRotate, cmap="gray")
plt.show()

imgMirror=np.fliplr(img)
plt.imshow(imgMirror, cmap="gray")
plt.show()