import numpy as np
import matplotlib.pyplot as plt

data = np.genfromtxt("lv2\data.csv", delimiter=",")
data = np.delete(data, 0, 0)
print("Mjerenje je izvrseno na "+str(np.shape(data)[0]))

plt.scatter([i[1] for i in data], [i[2] for i in data])
plt.show()
plt.scatter([i[1] for i in data[::50]], [i[2] for i in data[::50]])
plt.show()
visina = np.array([i[1] for i in data])
print("Minimalna visina je "+str(visina.min()))
print("Maksimalna visina je "+str(visina.max()))
print("Srednja visina je "+str(visina.mean()))
indM = (data[:,0] == 1)
visinaM = np.array([[]])
visinaZ = np.array([[]])
for i, el in enumerate(indM):
    if el == True:
        visinaM = np.append(visinaM, data[i][1])
    else:
        visinaZ = np.append(visinaZ, data[i][1])
print("Minimalna visina je "+str(visinaM.min()))
print("Maksimalna visina je "+str(visinaM.max()))
print("Srednja visina je "+str(visinaM.mean()))
print("Minimalna visina je "+str(visinaZ.min()))
print("Maksimalna visina je "+str(visinaZ.max()))
print("Srednja visina je "+str(visinaZ.mean()))