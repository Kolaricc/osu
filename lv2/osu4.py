import numpy as np
import matplotlib.pyplot as plt

black = np.zeros((50,50))
white = np.ones((50,50))
img = np.hstack((black,white))
img = np.vstack((img,np.hstack((white,black))))
plt.figure()
plt.imshow(img, cmap="gray")
plt.show()