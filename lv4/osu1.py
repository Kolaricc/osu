from math import sqrt
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import sklearn.linear_model as lm
from sklearn.metrics import mean_absolute_error, mean_squared_error, mean_absolute_percentage_error, r2_score

#a)
data = pd.read_csv("data_C02_emission.csv")

input_variables = ['Fuel Consumption City (L/100km)',
                   'Fuel Consumption Hwy (L/100km)',
                   'Fuel Consumption Comb (L/100km)',
                   'Fuel Consumption Comb (mpg)',
                   'Engine Size (L)',
                   'Cylinders']

output_variables = ['CO2 Emissions (g/km)']
X = data[input_variables].to_numpy()
y = data[output_variables].to_numpy()


# podijeli skup na podatkovni skup za ucenje i poda tkovni skup za testiranje
X_train , X_test , y_train , y_test = train_test_split (X , y , test_size = 0.2 , random_state =1 )

#b)
plt.scatter([i[0] for i in X_train],y_train,color = "blue", s=10)
plt.scatter([i[0] for i in X_test],y_test,color = "red", s=10, alpha = 0.5)
plt.xlabel ('Fuel Consumption City (L/100km)')
plt.ylabel('CO2 Emissions (g/km)')
plt.show()

plt.scatter([i[1] for i in X_train],y_train,color = "blue", s=10)
plt.scatter([i[1] for i in X_test],y_test,color = "red", s=10, alpha = 0.5)
plt.xlabel ('Fuel Consumption Hwy (L/100km)')
plt.ylabel('CO2 Emissions (g/km)')
plt.show()

plt.scatter([i[2] for i in X_train],y_train,color = "blue", s=10)
plt.scatter([i[2] for i in X_test],y_test,color = "red", s=10, alpha = 0.5)
plt.xlabel ('Fuel Consumption Comb (L/100km)')
plt.ylabel('CO2 Emissions (g/km)')
plt.show()

plt.scatter([i[3] for i in X_train],y_train,color = "blue", s=10)
plt.scatter([i[3] for i in X_test],y_test,color = "red", s=10, alpha = 0.5)
plt.xlabel ('Fuel Consumption Comb (mpg)')
plt.ylabel('CO2 Emissions (g/km)')
plt.show()

plt.scatter([i[4] for i in X_train],y_train,color = "blue", s=10)
plt.scatter([i[4] for i in X_test],y_test,color = "red", s=10, alpha = 0.5)
plt.xlabel ('Engine Size (L)')
plt.ylabel('CO2 Emissions (g/km)')
plt.show()

plt.scatter([i[5] for i in X_train],y_train,color = "blue", s=10)
plt.scatter([i[5] for i in X_test],y_test,color = "red", s=10, alpha = 0.5)
plt.xlabel ('Cylinders')
plt.ylabel('CO2 Emissions (g/km)')
plt.show()

#c)
sc = StandardScaler()
X_train_n = sc.fit_transform ( X_train )
X_test_n = sc.transform ( X_test )

plt.hist([i[0] for i in X_train])
plt.show()
plt.hist([i[0] for i in X_train_n])
plt.show()

#d)
linearModel = lm.LinearRegression()
linearModel.fit(X_train_n, y_train)
print(linearModel.coef_)

#e)
y_test_p = linearModel.predict ( X_test_n )

plt.scatter([i[0] for i in X_test],y_test,color = "blue", s=10)
plt.scatter([i[0] for i in X_test],y_test_p,color = "red", s=10, alpha = 0.5)
plt.show()

#f)qq
MSE = mean_squared_error (y_test, y_test_p)
RMSE = sqrt(MSE)
MAE = mean_absolute_error (y_test, y_test_p)
MAPE = mean_absolute_percentage_error (y_test, y_test_p)
R2 = r2_score(y_test, y_test_p)
print()

#g) Smanjenjem broja tesnih podataka, postotak greske se povecava