import pandas as pd
import numpy as np

data = pd.read_csv("data_C02_emission.csv")

#a) 
print ("Izvršeno je "+str(len(data))+" mjerenja")
print(data.info())

print("Postoje duplikati "+str(data.duplicated().values.any()))
if data.duplicated().values.any():
    data.drop_duplicates()
print("Postoje duplikati? "+str(data.isnull().values.any()))
if data.isnull().values.any():
    data.dropna()

dtypes = data.dtypes.to_dict()
for col_name, typ in dtypes.items():
    if (typ == "object"): 
        data[col_name] = data[col_name].astype("category")
        
#b)
sortdata = data.sort_values(by=["Fuel Consumption City (L/100km)"])
print("Automobili sa najmanjom potrosnjom: ")
print(sortdata.iloc[0:3, [0,1,7]])
print("Automobili sa najvecom potrosnjom: ")
print(sortdata.iloc[-3:, [0,1,7]])

#c)
motor =  data[(data["Engine Size (L)"] >= 2.5 ) & (data["Engine Size (L)"] <= 3.5 )]
print("Postoje "+str(len(motor))+" vozila sa motorom izmedju 2.5 i 3.5")
print("Prosjecna CO2 emisija ovih vozila je: "+str(motor["CO2 Emissions (g/km)"].mean()))

#d)
audi = data[(data["Make"] == "Audi")]
print("Audi vozila: "+str(len(audi)))
print("Prosjecna CO2 emisija audija je: "+str(audi[(audi["Cylinders"] == 4)]["CO2 Emissions (g/km)"].mean()))

#e)
cylinders = data.sort_values(by=["Cylinders"])
print(cylinders.groupby(["Cylinders"]).count().iloc[:,1])
print(cylinders.groupby(["Cylinders"])["CO2 Emissions (g/km)"].mean())

#f)
dizel = data[(data["Fuel Type"] == "D")]
print("Dizel prosjecna gradska potrosnja: "+str(dizel["Fuel Consumption City (L/100km)"].mean()))
Rbenzin = data[(data["Fuel Type"] == "X")]
print("Regularni benzin prosjecna gradska potrosnja: "+str(Rbenzin["Fuel Consumption City (L/100km)"].mean()))
print("Dizel medija: "+str(dizel["Fuel Consumption City (L/100km)"].median()))
print("Regularni benzin medija: "+str(Rbenzin["Fuel Consumption City (L/100km)"].median()))

#g)
print(data[(data["Cylinders"] == 4) & (data["Fuel Type"] == "D") & (data["Fuel Consumption City (L/100km)"] == data[(data["Cylinders"] == 4) & (data["Fuel Type"] == "D")]["Fuel Consumption City (L/100km)"].max())])

#h)
rucni = len([ind for ind in data["Transmission"] if ind[0] == "M"])
print(rucni)

#i)Izracunajte korelaciju izmedu numerickih velicina. Komentirajte dobiveni rezultat.
