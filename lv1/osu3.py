lista = []

def Average(lst): 
    return sum(lst) / len(lst)

while 1:
    unos = input("Unesi broj: ")
    if unos == "Done":
        break
    try:
        broj = int(unos)
    except ValueError:
        print("Unos nije broj!")
        continue
    lista.append(broj)
print("Korisnik je unio "+str(len(lista))+" broja. Njihova srednja vrijednost je "+str(Average(lista))+". Najmanji broj je "+str(min(lista))+", a najveci "+str(max(lista)))

lista.sort()
print(lista)