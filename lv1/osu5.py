spam = []
ham = []
spamWordCount = []
hamWordCount = []
exclamationCount = 0

def Average(lst): 
    lenghts = []
    for line in lst:
        words = line.split()
        lenghts.append(len(words))
    return sum(lenghts) / len(lenghts)


sms = open("SMSSpamCollection.txt", encoding="utf-8")
for line in sms:
    line = line.rstrip()
    words = line.split()
    if words[0] == "spam":
        line = line.lstrip("spam\t")
        spam.append(line)
    elif words[0] == "ham":
        line = line.lstrip("ham\t")
        ham.append(line)
sms.close()

for line in ham:
    words = line.split()
    hamWordCount.append(len(words))

for line in spam:
    words = line.split()
    spamWordCount.append(len(words))
    if line[-1] ==  "!":
        exclamationCount += 1

print("Prosjecan broj rijeci u ham je "+str(Average(ham)))#14
print("Prosjecan broj rijeci u spam je "+str(Average(spam)))#16 ili 23
print("Spam poruke koje zavrsavaju sa ! "+str(exclamationCount))#51