try:
    ocjena = float(input("Unesite ocjenu od 0.0 do 1.0: "))
except ValueError:
    print("Broj nije unesen")
if(ocjena>1 or ocjena<0):
    print("Ocjena je izvan granica [0,1]")
elif(ocjena>=0.9):
    print("A")
elif(ocjena>=0.8):
    print("B")
elif(ocjena>=0.7):
    print("C")
elif(ocjena>=0.6):
    print("D")
else:
    print("F")